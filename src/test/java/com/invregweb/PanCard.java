package com.invregweb;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import com.investorregmain.InvestorRegistrationWebTest;
import com.lendenclub.data.ExcelSheet;

public class PanCard
{
	String pancard="JVUPS4433A";
	
	public void page002PanCard()
	{   
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println("hello");
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='PAN Number']")).sendKeys(ExcelSheet.ReadCell(ExcelSheet.GetCell("Investor_Details"), 4));
		System.out.println("reached");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		InvestorRegistrationWebTest.driver.findElement(By.cssSelector("input[placeholder='DD/MM/YYYY']")).sendKeys("12071999");
		InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div/div[6]/div")).click(); 
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Chooseplan
		InvestorRegistrationWebTest.driver.findElement(By.className("plan-btn-label")).click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
