package com.investorregmain;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.invregweb.BankDetails;
import com.invregweb.BasicDetails;
import com.invregweb.ClickCheckbox;
import com.invregweb.LiveKYC;
import com.invregweb.Login;
import com.invregweb.PanCard;
import com.invregweb.Payment;
import com.lendenclub.data.ExcelSheet;
import com.lendenclub.data.Screenshot;

import io.github.bonigarcia.wdm.WebDriverManager;

@Listeners(Screenshot.class)
public class InvestorRegistrationWebTest extends Screenshot {
	public static WebDriver driver;
	public static ExcelSheet excel;

	public ExcelSheet getExcel() {

		return super.getExcel();
	}


	public static WebDriver getDriver() {
		return driver;
	}
	public static void setDriver(WebDriver driver) {
		InvestorRegistrationWebTest.driver = driver;
	}



	Login login=new Login();
	PanCard pancard=new PanCard();
	Payment payment=new Payment();
	LiveKYC livekyc=new LiveKYC();
	ClickCheckbox check=new ClickCheckbox();
	BasicDetails bs=new BasicDetails();
	BankDetails bd=new BankDetails();


	@BeforeTest
	public void setup()
	{
		ChromeOptions opt = new ChromeOptions();
		WebDriverManager.chromedriver().setup();
		opt.addArguments("headless");
		driver = new ChromeDriver(opt);
	}

	@Test(priority = 1)
	public void test001Login() throws InterruptedException
	{
		login.page001Login();
		new Login();
	} 
	@Test(priority = 2)
	public void test002PanCard()
	{
		pancard.page002PanCard();
	}
	@Test(priority = 3)
	public void test003Payment()
	{
		payment.page003Payment();
	} 

	@Test(priority = 4)
	public void test004ClickCheckbox() throws Exception
	{
		check.page004ClickCheckbox();
	}
	@Test(priority = 5)
	public void test005LiveKYC() throws InterruptedException
	{
		livekyc.page005LiveKYC();
	}

	@Test(priority = 6)
	public void test006BasicDetails() throws InterruptedException
	{
		bs.page006BasicDetails();
	}
	@Test(priority = 7)
	public void test007BankDetails()
	{
		bd.page007BankDetails();
	}



}



